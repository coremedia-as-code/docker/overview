FROM nginx:1-alpine

ENV STUDIO_PACKAGES_PROXY_ENABLED=false \
    PWA_ENABLED=false

COPY entrypoint.sh /entrypoint.sh

RUN apk add --update \
      curl \
      bash && \
    rm -rf /var/cache/apk/* && \
    curl -sL "https://github.com/kelseyhightower/confd/releases/download/v0.16.0/confd-0.16.0-linux-amd64" -o /usr/bin/confd && \
    chmod +x /usr/bin/confd && /bin/chmod 775 /entrypoint.sh

COPY confd /etc/confd
COPY index.html /usr/share/nginx/html/
COPY assets /usr/share/nginx/html/assets
COPY conf.d /etc/nginx/conf.d

ENTRYPOINT ["/entrypoint.sh"]
